package main

import (

	"time"
)

type ErrorResponse struct {
	Error string `json:"error"`
}

type Versions struct {
	Version   string `json:"version"`
	Commit    string `json:"commit"`
	BuildTime string `json:"buildTime"`
}

type Book struct {
	Authors []Author `json:"authors"`
}

type Author struct {
	Key string `json:"key"`
}

type Name struct {
	Name string `json:"personal_name"`
}

type RespAuthor struct {
	Name string `json:"name"`
	Key  string `json:"authorKey"`
}

type Entries struct {
	Entries []Books `json:"entries"`
}
type Books struct {
	Title    string `json:"title"`
	Key      string `json:"key"`
	Revision int    `json:"revision"`
	Created  struct {
		Value string `json:"value"`
	} `json:"created"`
}
type RespBooks struct {
	Name        string `json:"name"`
	Key         string `json:"key"`
	Revision    int    `json:"revision"`
	PublishDate string `json:"publishDate"`
}

type LogRecord struct {
	Time                                time.Time
	Ip, Method, Uri, Protocol, Username string
}




type Config struct{
	Server	 		*server		`mapstructure:"server" validate:"required"`
	Openlibrary	 	openlibrary	`mapstructure:"openLibrary"`
	//Censors			Censors		`mapstructure:"censors" validate:"required,dive,startswith=OL"`
	Censors			Censors		`mapstructure:"censors" validate:"required,dive,myValidator"`
}


type server struct		{
	Addr		string	`mapstructure:"addr" validate:"required"`
	BaseApiUrl	string	`mapstructure:"baseApiUrl" validate:"startswith=/,required"`
	SwaggerUrl	string	`mapstructure:"swaggerUrl" validate:"startswith=/,required"`
	MetricsUrl	string	`mapstructure:"metricsUrl" validate:"startswith=/,required"`
}


type openlibrary	struct{
	BaseUrl 	string	`mapstructure:"baseUrl"`
	ResultLimit	uint16	`mapstructure:"resultLimit" validate:"gte=2,lte=130"`

}

type Censors []string





//response: http 200 ok body: [{"name": "name" ,"/key": "OL23232", "revisions" : 5, "publishDate" : "RFC3339" }]

/*package main

type ErrorResponse struct {
	Error string `json:"Error"`
}

type Version struct {
	Version   string `json:"version"`
	Commit    string `json:"commit"`
	BuildTime string `json:"buildTime"`
}

/*type JsonResults struct {
	Title   string `json:"title"`
	Authors []struct {
		Key string `json:"key"`
	} `json:"authors"`
}



type Book struct {
	Authors []Author `json:"authors"`
}

type Author struct {
	Key string `json:"key"`
}

type AuthorResponse struct {
	Name         string `json:"name"`
	FullName     string `json:"fuller_name"`
	PersonalName string `json:"personal_name"`
}

type EndpointAuthorResponse struct {
	AuthorName string `json:"author"`
	Key        string `json:"authorKey"`
}


// nakopcene odial endpoint3
type Entries struct{
	Works []Work  `json:"entries"`

}

type Work struct{
	ID string `json:"key"`
	Revision string `json:"key"`
	Name string `json:"title"`
}
*/