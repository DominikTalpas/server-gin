build:
	GOOS=linux GOARCH=amd64 CGO_ENABLED=0  go build -ldflags "-X main.version=v1.0.0 -X main.commit=$(shell git rev-parse HEAD) -X 'main.buildtime=$(shell date --rfc-3339=second)'"

run:
	go build -o ${BINARY_NAME} -ldflags "-X 'main.appVersion=v1.0.0' -X 'main.commit=$(shell git rev-parse HEAD)' -X 'main.buildTime=$(shell date --rfc-3339=second)'"
	./${BINARY_NAME}

clean:
	go clean
