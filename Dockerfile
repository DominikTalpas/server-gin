FROM golang:alpine AS builder

WORKDIR /app

COPY . .

RUN go build -o app .

EXPOSE 8080

ENTRYPOINT ["/app/app"]