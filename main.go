package main

import (
	"context"
	"encoding/json"
	"errors"
	

	"io/ioutil"

	"strconv"
	"strings"

	//"encoding/json"
	//"errors"
	"fmt"
	"os/signal"
	"sync"
	"syscall"
	"time"

	swaggerfiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"

	//"io"
	//"io/ioutil"
	"log"
	"net/http"

	//"strings"
	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"github.com/spf13/viper"
)

var appVersion, commit, buildTime string
var versions Versions

// var censored map[string]bool
var censored = make(map[string]bool)

var mux sync.Mutex

var config Config


var (
	httpRequestCounter = promauto.NewCounterVec(prometheus.CounterOpts{
			Name: "http_request_total",
			Help: "counterhttp request",
	},
		[]string{"code","markiza"},
)
)
var validate *validator.Validate


// @title           Swagger Example API
// @version         1.0
// @description     This is a sample server celler server.
// @termsOfService  http://swagger.io/terms/

// @contact.name   API Support
// @contact.url    http://www.swagger.io/support
// @contact.email  support@swagger.io

// @license.name  Apache 2.0
// @license.url   http://www.apache.org/licenses/LICENSE-2.0.html

// @host      :8080
// @BasePath  /api/v1




func main() {

	//configPath :=flag.String("config","config.yaml","dsfsfsfsf")



	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
    viper.AddConfigPath("/home/dtalpas/Desktop/go/server-gin/")
    if err := viper.ReadInConfig();  err != nil {
        return
    }

    if err := viper.Unmarshal(&config); err != nil {
        fmt.Println(err)
        return
    }
    fmt.Println(config)

	validate = validator.New()
	validate.RegisterValidation("myValidator", myValidator)

	err := validateStruct(&config)
	if  err != nil{
		log.Fatal(err)
	}



	versions.Version = appVersion
	versions.Commit = commit
	versions.BuildTime = buildTime
	fmt.Println(versions.BuildTime, versions.Commit, versions.Version)

	router := gin.New()
	router.Use(Metric)
	router.GET(config.Server.MetricsUrl, gin.WrapH(promhttp.Handler()))


	router.GET(config.Server.BaseApiUrl + "/v1/version", version)
	router.GET(config.Server.BaseApiUrl + "/v1/author", getAuthors)
	router.GET(config.Server.BaseApiUrl + "/v1/book", getBooks)
	router.POST(config.Server.BaseApiUrl + "/v1/censors", endpoint4)
	router.GET(config.Server.SwaggerUrl +"/*any", ginSwagger.WrapHandler(swaggerfiles.Handler))

	ctx, stop := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM)
	defer stop()

	srv := &http.Server{
		Addr:    config.Server.Addr,
		Handler: router,
	}

	go func() {
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()

	// Listen for the interrupt signal.
	<-ctx.Done()

	stop()
	log.Println("shutting down gracefully, press Ctrl+C again to force")

	// The context is used to inform the server it has 5 seconds to finish
	// the request it is currently handling
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server forced to shutdown: ", err)
	}

	log.Println("Server exiting")

	//log.Fatal(http.ListenAndServe(":8080", nil))




}

//  @Summary      version
//  @Description  version , commit , buidtime
//	@Router       /version [get]

func version(c *gin.Context) {

	c.IndentedJSON(http.StatusOK, Versions{Version: versions.Version, Commit: versions.Commit, BuildTime: versions.BuildTime})

}

func Metric(c *gin.Context){

	c.Next()
	Counter, err:=httpRequestCounter.GetMetricWith(prometheus.Labels{
		"code": strconv.Itoa( c.Writer.Status()),
		"markiza": c.Request.URL.Path ,
	})

	httpRequestCounter.GetMetricWithLabelValues()
	if err != nil{
		log.Println(err)
	}


	Counter.Inc()

}

func getAuthors(c *gin.Context) {
	bookAuthors := Book{}
	name := Name{}

	Id := c.Query("book")

	url := config.Openlibrary.BaseUrl + "/isbn/" + Id + ".json"

	errorstatus := http.StatusOK
	defer AccesLog(c.Request, errorstatus)

	body, err := apiCall(url)
	if err != nil {
		sendError(c.Writer, http.StatusInternalServerError, err)
		errorstatus = http.StatusInternalServerError
		return
	}

	err = json.Unmarshal(body, &bookAuthors)
	if err != nil {
		fmt.Println("error:", err)
		sendError(c.Writer, http.StatusInternalServerError, errors.New("cannot create error"))
	}

	respA := make([]RespAuthor, len(bookAuthors.Authors))

	for index := range bookAuthors.Authors {
		url = config.Openlibrary.BaseUrl + bookAuthors.Authors[index].Key + ".json"
		body, err = apiCall(url)
		if err != nil {
			sendError(c.Writer, http.StatusInternalServerError, err)
			errorstatus = http.StatusInternalServerError
			return
		}
		err := json.Unmarshal(body, &name)
		if err != nil {
			fmt.Println("error:", err)
			sendError(c.Writer, http.StatusInternalServerError, errors.New("cannot create error"))
			errorstatus = http.StatusInternalServerError
		}

		bookAuthors.Authors[index].Key = strings.ReplaceAll(bookAuthors.Authors[index].Key, "/authors/", "")

		respA[index].Name = name.Name
		respA[index].Key = bookAuthors.Authors[index].Key
	}

	c.IndentedJSON(http.StatusOK, respA)

}

func getBooks(c *gin.Context) {

	authorBooks := Entries{}

	errorstatus := http.StatusOK
	defer AccesLog(c.Request, errorstatus)

	author := c.Query("author")

	url := config.Openlibrary.BaseUrl + "/authors/" + author + "/works.json?limit=" + strconv.FormatUint(uint64(config.Openlibrary.ResultLimit),10)

		if readcensored(author) {
			sendError(c.Writer, http.StatusBadRequest, errors.New("author is blocked"))
			errorstatus = http.StatusBadRequest
			return

		}


	body, err := apiCall(url)
	if err != nil {
		sendError(c.Writer, http.StatusInternalServerError, err)
		return
	}

	err = json.Unmarshal(body, &authorBooks)
	if err != nil {
		fmt.Println("error:", err)
		sendError(c.Writer, http.StatusInternalServerError, errors.New("cannot create error"))
		errorstatus = http.StatusInternalServerError
	}

	respB := make([]RespBooks, len(authorBooks.Entries))

	for index := range authorBooks.Entries {

		respB[index].Name = authorBooks.Entries[index].Title
		respB[index].Key = authorBooks.Entries[index].Key
		respB[index].Revision = authorBooks.Entries[index].Revision
		respB[index].PublishDate = authorBooks.Entries[index].Created.Value
	}

	c.IndentedJSON(http.StatusOK, respB)

}

func apiCall(url string) ([]byte, error) {
	resp, err := http.Get(url)
	if err != nil {
		log.Println(err)
	}

	defer resp.Body.Close()

	// !!! nacitava cely []byte do pamate, lepsie bufio.NewReader()
	body, err := ioutil.ReadAll(resp.Body)

	return body, err
}

func sendError(w http.ResponseWriter, httpCode int, err error) {
	buf, locErr := json.Marshal(ErrorResponse{Error: err.Error()})
	if locErr != nil {
		log.Println("origin error", err)
		log.Println("json marshall error", locErr)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.WriteHeader(httpCode)
	w.Write(buf)
}

/*
// merge request try
//curl localhost:8080/api/v1/book?author=OL23919B
*/
func endpoint4(c *gin.Context) {

	/*errorstatus := http.StatusOK
	//ptrerrrorstatus = &errorstatus
	defer AccesLog(c.Request, errorstatus)
*/
	censors2 := make([]string, 0)

	err := c.BindJSON(&censors2)
	if err != nil {
		log.Println()
		c.IndentedJSON(http.StatusInternalServerError, ErrorResponse{Error: "Internal error"})
		http.Error(c.Writer, err.Error(), http.StatusBadRequest)
		return
	}

	addcensored(censors2)

	c.IndentedJSON(http.StatusOK, "authors added to censors list")

	censors3 := make([]string, 0)

	for c:= range censored{
		censors3 = append(censors3, c)
	}

	viper.Set("censors",censors3)
	viper.WriteConfig()

	//body, _ := io.ReadAll(req.Body)
	//fmt.Fprintf(w, string(body))

}


func addcensored(censors2 []string) {

	mux.Lock()
	for _, c := range censors2 {
		censored[c] = true
	}
	mux.Unlock()
}

func readcensored(author string) bool {
	mux.Lock()
	defer mux.Unlock()
	return censored[author]

}

func AccesLog(req *http.Request, statuscode int) {

	fmt.Println(time.Now(), req.Method, req.URL, req.RemoteAddr, statuscode)

}




func	validateStruct(*Config)error{
	err := validate.Struct(config)
	if err != nil {

		// this check is only needed when your code could produce
		// an invalid value for validation such as interface with nil
		// value most including myself do not usually have code like this.
		if _, ok := err.(*validator.InvalidValidationError); ok {
			fmt.Println(err)
			return err
		}
		for _, err := range err.(validator.ValidationErrors) {

			fmt.Println(err.Namespace())
			fmt.Println(err.Field())
			fmt.Println(err.StructNamespace())
			fmt.Println(err.StructField())
			fmt.Println(err.Tag())
			fmt.Println(err.ActualTag())
			fmt.Println(err.Kind())
			fmt.Println(err.Type())
			fmt.Println(err.Value())
			fmt.Println(err.Param())
			fmt.Println()
		}


		return err
	}
  return nil
}

func myValidator(fl validator.FieldLevel) bool {
	nb := fl.Field().String()

	return strings.HasPrefix(nb, "OL")

}

/*package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"

	//"io/ioutil"
	"log"
	"net/http"

	//"os"
	"strings"
)

var (
	version, commit, buildtime string
	appVersions                Version
)

func sendError(w http.ResponseWriter, httpCode int, err error) {
	buf, locErr := json.Marshal(ErrorResponse{Error: err.Error()})
	if locErr != nil {
		log.Println("origin error", err)
		log.Println("json marshall error", locErr)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.WriteHeader(httpCode)
	w.Write(buf)
}

func getVersion(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		f, err := json.Marshal(ErrorResponse{Error: "unsaported method, suported method is GET"})
		if err != nil {
			fmt.Println("error:", err)
			w.WriteHeader(http.StatusBadRequest)
		}
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(f))
		return
	} else {
		g, _ := json.Marshal(Version{Version: appVersions.Version, Commit: appVersions.Commit, BuildTime: appVersions.BuildTime})
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(g))
	}

	fmt.Fprintf(w, "Hi there, I love %s!", r.URL.Path[1:])
}

func getauthors(w http.ResponseWriter, r*http.Request) {
	var authorSearch JsonResults

	if r.Method != http.MethodGet {
		f, err := json.Marshal(ErrorResponse{Error: "unsaported method, suported method is GET"})
		if err != nil {
			fmt.Println("error:", err)
			w.WriteHeader(http.StatusBadRequest)
		}
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(f))
		return
	}


    book := r.URL.Query().Get("book")

	if book == ""{
		f, err := json.Marshal(ErrorResponse{Error: "missing book variable"})
		if err != nil {
			fmt.Println("error:", err)
			w.WriteHeader(http.StatusBadRequest)
		}
		w.WriteHeader(http.StatusBadRequest)
		w.Write(f)
		return
	}else{

	http_query := "https://openlibrary.org/authors/" + book + ".json"
	resp, err := http.Get(http_query)
		if err != nil {
			log.Print(err)
			fmt.Print(err.Error())
			os.Exit(1)
			}

	responseData, err := ioutil.ReadAll(resp.Body)

		if err := json.Unmarshal(responseData, &authorSearch)
		err != nil {
			log.Fatal(err)
			os.Exit(1)
			}

	result, err := json.Marshal(authorSearch)
		if err != nil {
			log.Print(err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		w.WriteHeader(http.StatusOK)
		w.Write(result)



	}
}


func getAuthors(w http.ResponseWriter, req *http.Request) {
	if req.Method != http.MethodGet {
		sendError(w, http.StatusBadRequest, errors.New("unsupported method. GET method is supported only"))
		return
	}

	bookISBN := req.URL.Query().Get("book")
	if bookISBN == "" {
		sendError(w, http.StatusBadRequest, errors.New("missing URL param book which has to contain isbn of book"))
		return
	}

	authors, err := getAuthorsKey(bookISBN)
	if err != nil {
		sendError(w, http.StatusInternalServerError, errors.New("cannot find author of the book"))
		return
	}

	result := make([]EndpointAuthorResponse, len(authors))
	for index := range authors {
		result[index].AuthorName, err = getAuthorsName(authors[index].Key)
		if err != nil {
			sendError(w, http.StatusInternalServerError, err)
			return
		}
		result[index].Key = strings.TrimPrefix(authors[index].Key, "/authors/")
	}

	buf, err := json.Marshal(result)
	if err != nil {
		log.Println(err)
		sendError(w, http.StatusInternalServerError, errors.New("cannot create error"))
	}

	_, err = w.Write(buf)
	if err != nil {
		log.Println(err)
	}
}

func getAuthorsKey(isbn string) ([]Author, error) {
	resp, err := http.Get("https://openlibrary.org/isbn/" + isbn + ".json")
	if err != nil {
		return nil, err
	}

	defer close(resp.Body)

	decoder := json.NewDecoder(resp.Body)

	book := Book{}
	err = decoder.Decode(&book)
	if err != nil {
		return nil, err
	}

	return book.Authors, nil
}

func getAuthorsName(key string) (string, error) {
	resp, err := http.Get("https://openlibrary.org" + key + ".json")
	if err != nil {
		return "", nil
	}

	defer close(resp.Body)

	decoder := json.NewDecoder(resp.Body)

	author := AuthorResponse{}
	err = decoder.Decode(&author)
	if err != nil {
		return "", err
	}

	if author.FullName != "" {
		return author.FullName, nil
	} else if author.PersonalName != "" {
		return author.PersonalName, nil
	}

	return author.Name, nil
}

func close(reader io.ReadCloser) {
	err := reader.Close()
	if err != nil {
		log.Println(err)
	}
}














// nakopcene odtial



func getBooks(w http.ResponseWriter, req *http.Request) {
	if req.Method != http.MethodGet {
		sendError(w, http.StatusBadRequest, errors.New("unsupported method. GET method is supported only"))
		return
	}

	authorId := req.URL.Query().Get("author")
	if authorId == "" {
		sendError(w, http.StatusBadRequest, errors.New("missing URL param works which has to contain ID of author"))
		return
	}

	works, err := getWorksID(authorId)
	if err != nil {
		sendError(w, http.StatusInternalServerError, errors.New("cannot find author of the book"))
		return
	}


}
func getWorksID(authorId string) ([]Work, error) {
	resp, err := http.Get("https://openlibrary.org/authors/" + authorId + "works.json")
	if err != nil {
		return nil, err
	}

	defer close(resp.Body)

	decoder := json.NewDecoder(resp.Body)

	auth := Entries{}
	err = decoder.Decode(&auth)
	if err != nil {
		return nil, err
	}

	return auth.Works, nil

	fmt.Println()
}
//potail








func main() {
	appVersions.Version = version
	appVersions.Commit = commit
	appVersions.BuildTime = buildtime

	fmt.Println(appVersions)

	//http.HandleFunc("/api/v1/version", getVersion)
	//http.HandleFunc("/api/v1/authors", getAuthors)
	http.HandleFunc("/api/v1/authors", getBooks)

	log.Fatal(http.ListenAndServe(":8080", nil))
}


*/
